class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.belongs_to :people, index:  true
      t.string :street
      t.string :suite
      t.string :city
      t.string :zipcode

      t.timestamps
    end
  end
end
