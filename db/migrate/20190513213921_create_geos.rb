class CreateGeos < ActiveRecord::Migration[5.2]
  def change
    create_table :geos do |t|
      t.belongs_to :address, index: true
      t.string :lat
      t.string :lng

      t.timestamps
    end
  end
end
