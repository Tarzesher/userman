class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :name
      t.string :username
      t.string :email
      t.integer :address_id
      t.string :phone
      t.string :website

      t.timestamps
    end
  end
end
