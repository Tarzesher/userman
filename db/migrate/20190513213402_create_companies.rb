class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.belongs_to :people, index: true
      t.string :name
      t.string :catchPhrase
      t.string :bs

      t.timestamps
    end
  end
end
