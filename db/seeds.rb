# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'rest_client'
require 'json'

url = 'https://jsonplaceholder.typicode.com/users'
response = RestClient.post(url, :content_type => 'application/json')

json.each do |a|
  Person.create!(a, without_protection: true)
  Company.create!(a['company'], without_protection: true)
  Address.create!(a['address'], without_protection: true)
  Geo.create!(a['address']['geo'], without_protection: true)
end