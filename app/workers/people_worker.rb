class PeopleWorker
  include Sidekiq::Worker

  def perform(*args)
    if params[:search]
      @people = People.search(params[:search]).order("created_at DESC")
    else
      @people = People.all.order('created_at DESC')
    end
  end
end
